<?php

namespace Zaralytics;

/**
 * @author Pavol Eichler
 */
class Event {
   
    /**
     * Array format.
     */
    const FORMAT_ARRAY = 'array';
    /**
     * String format.
     */
    const FORMAT_STRING = 'string';
    /**
     * JSON format.
     */
    const FORMAT_JSON = 'json';
    
    /**
     * The event user.
     * @var User
     */
    protected $user;
    
    /**
     * The event category.
     * @var array
     */
    protected $category;
    /**
     * The event name.
     * @var string
     */
    protected $event;
    
    /**
     * The event values.
     * 
     * @var array
     */
    protected $values;

    /**
     * @param \Zaralytics\User $user Event user.
     * @param array|string $category Event category as a string ('login/facebook') or an array (array('login', 'facebook')).
     * @param string $event Event name.
     * @param array $values An associative array of event related data. String or nummeric data values are allowed only.
     */
    public function __construct(User $user, $category, $event, $values = array()) {
        
        // convert $values evaluating to false to an empty array
        $values = $values ? $values : array();
        
        // parse the category value and get an array of categories
        $category = $this->parseCategory($category);
        
        // validate arguments
        $this->validateCategory($category);
        $this->validateEvent($event);
        $this->validateValues($values);
        
        // assign data
        $this->user = $user;
        $this->category = $category;
        $this->event = (string) $event;
        $this->values = $values;
        
    }

    /**
     * Get the event user.
     * 
     * @return User
     */
    public function getUser() {
        
        return $this->user;
        
    }

    /**
     * Get the event category.
     * 
     * @param string $format Response format. Use one of the available class constants.
     * @return array|string
     */
    public function getCategory($format = self::FORMAT_ARRAY) {
        
        switch ($format) {
            case self::FORMAT_STRING:
                return implode('/', $this->category);
            case self::FORMAT_JSON:
                return json_encode($this->category);
            default:
                return $this->category;
        }
        
    }

    /**
     * Get the event name.
     * 
     * @return string
     */
    public function getName() {
        
        return $this->event;
        
    }

    /**
     * Get the event values.
     * 
     * @return array
     */
    public function getValues() {
        
        return $this->values;
        
    }
    
    /**
     * Validates the category format.
     * 
     * @param array $category
     * @throws \Exception If the category format is invalid.
     */
    protected function validateCategory($category) {
        
        if (!is_array($category)){
            throw new \Exception('Category must be an array.');
        }
        
        // loop through the whole tree and validate individual category names
        foreach($category as $cat){
            if (!$cat){
                throw new \Exception('One of the category names is an empty string.');
            }
        }
        
    }
    
    /**
     * Parse category data.
     * 
     * @param array|string $category
     * @return array
     */
    protected function parseCategory($category) {
        
        if (is_array($category))
            return $category;
        
        // trim slashes
        $category = trim($category, '/');
        
        return explode('/', $category);
        
    }
    
    /**
     * Validates the event name format.
     * 
     * @param string $event
     * @throws \Exception If the event name format is invalid.
     */
    protected function validateEvent($event) {
        
        $event = (string) $event;
        
        if (!is_string($event) OR !$event){
            throw new \Exception('Event must be a non-empty string.');
        }
        
    }
    
    /**
     * Validates event values format.
     * 
     * @param array $values
     * @throws \Exception If the event values format is invalid.
     */
    protected function validateValues($values) {
        
        if (!is_array($values)){
            throw new \Exception('Values must be an array.');
        }
        
        foreach($values as $key => $value){
            if ($value !== null AND
                !is_bool($value) AND
                !is_numeric($value) AND
                !is_string($value)){
                throw new \Exception('Event values can be either boolean or numeric values or strings.');
            }
        }
        
    }
    
}