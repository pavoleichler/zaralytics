<?php

namespace Zaralytics;

/**
 * @author Pavol Eichler
 */
class Project {
    
    /**
     * Project UID.
     * 
     * @var string
     */
    protected $uid;

    /**
     * @param string $id
     */
    public function __construct($uid) {
        
        $this->uid = $uid;
        
    }
    
    /**
     * Get the current project UID.
     * 
     * @return string
     */
    public function getUid() {
        
        return $this->uid;
        
    }
    
    /**
     * Returns all object data as a JSON string.
     * 
     * @return string
     */
    public function toJson() {
        
        return json_encode(array(
            'uid' => $this->uid
        ));
        
    }
    
}