<?php

namespace Zaralytics;

/**
 * @author Pavol Eichler
 */
class Request {
   
    /**
     * Request ID.
     * 
     * @var type 
     */
    protected $uid;

    /**
     * 
     */
    public function __construct() {
        
        $this->uid = $this->generateUid();
        
    }
    
    /**
     * Get the current request UID.
     * 
     * @return string
     */
    public function getId() {
        
        return $this->uid;
        
    }
    
    /**
     * Set the current request UID.
     */
    public function setId($uid) {
        
        $this->uid = $uid;
        
    }
    
    /**
     * Generates a new pseudo-unique request UID.
     * 
     * @return string
     */
    protected function generateUid() {
        
        $prefix = php_uname('n');
        $prefix .= serialize($_SERVER);
        $prefix .= mt_rand();
        
        $uid = uniqid(md5($prefix) . '.', true);
        
        return $uid;
        
    }
    
}