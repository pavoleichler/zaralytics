<?php

namespace Zaralytics;

/**
 * @author Pavol Eichler
 */
class Referer {
    
    /**
     * Identifies a search engine, newsletter name, or other source.
     * E.g.: google
     */
    const SOURCE = 'source';
    /**
     * Identifies a medium such as email or cost-per-click.
     * E.g.: cpc
     */
    const MEDIUM = 'medium';
    /**
     * Identifies the keywords used for this ad.
     * E.g.: running+shoes
     */
    const TERM = 'term';
    /**
     * Identifies ads or links that point to the same URL in A/B testing and content-targeted ads.
     * E.g.: textlink or logolink
     */
    const CONTENT = 'content';
    /**
     * Identifies a specific product promotion or strategic campaign.
     * E.g.: spring_sale
     */
    const CAMPAIGN = 'campaign';
    
    /**
     * Name of the campaign source GET variable.
     * @var string
     */
    protected static $campaign_source = 'utm_source';
    /**
     * Name of the campaign medium GET variable.
     * @var string
     */
    protected static $campaign_medium = 'utm_medium';
    /**
     * Name of the campaign term GET variable.
     * @var string
     */
    protected static $campaign_term = 'utm_term';
    /**
     * Name of the campaign content GET variable.
     * @var string
     */
    protected static $campaign_content = 'utm_content';
    /**
     * Name of the campaign name GET variable.
     * @var string
     */
    protected static $campaign_campaign = 'utm_campaign';

    /**
     * The current referer value.
     * @var string
     */
    protected $referer;
    /**
     * The current campaign data.
     * @var array
     */
    protected $campaign;

    /**
     * @param string $referer Referer URL.
     * @param array $campaign Campaign data as an array with any or none of the following keys: self::SOURCE, self::MEDIUM, self::TERM, self::CONTENT, self::CAMPAIGN.
     */
    public function __construct($referer = null, $campaign = array()) {
        
        $this->referer = $referer;
        $this->campaign = $this->parseCampaign($campaign);
        
    }
    
    /**
     * Gets the current referer URL.
     * 
     * @return string
     */
    public function getReferer() {
        
        return $this->referer;
    
    }

    /**
     * Gets the current campaign data.
     * 
     * @param string $property One of the class constatnts to get one campaign property or null to get all campaign data.
     * @return array|string
     * @throws Exception If passed an invalid property.
     */
    public function getCampaign($property = null) {
        
        if ($property !== null AND
            $property !== self::CAMPAIGN AND
            $property !== self::CONTENT AND
            $property !== self::MEDIUM AND
            $property !== self::SOURCE AND
            $property !== self::TERM)
            throw new Exception('Invalid property name.');
        
        return $property === null ?
                // all data
                $this->campaign :
                // one property
                isset($this->campaign[$property]) ?
                    // property exists
                    $this->campaign[$property] :
                    // property does not exist
                    false;
        
    }
    
    /**
     * Returns all object data as a JSON string.
     * 
     * @return string
     */
    public function toJson(){
        
        return json_encode(array(
            'referer' => $this->referer,
            'campaign' => $this->campaign
        ));
        
    }
    
    /**
     * Parses the provided campaign data.
     * 
     * @param array $campaign
     * @return array
     * @throws Exception If campaign data format is invalid.
     */
    protected function parseCampaign($campaign) {
        
        // take values evalutaing to false as no campaign data
        if (!$campaign)
            return array();
        
        if (!is_array($campaign))
            throw new Exception('Please, provide the campaign data as an array.');
        
        // extract the relevant keys only
        return array_intersect_key($campaign, array(
            self::SOURCE => null,
            self::MEDIUM => null,
            self::TERM => null,
            self::CONTENT => null,
            self::CAMPAIGN => null
        ));
        
    }

    /**
     * Identifies the campaign data and the referer based on the HTTP request.
     * 
     * @return \Zaralytics\Referer
     */
    public static function identify(){
        
        // extract the refering URL
        $referer = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : null;
        
        // extract campaign UTM identifiers
        $campaign = array();
        if (isset($_GET[self::$campaign_source]))
            $campaign[self::SOURCE] = $_GET[self::$campaign_source];
        if (isset($_GET[self::$campaign_medium]))
            $campaign[self::MEDIUM] = $_GET[self::$campaign_medium];
        if (isset($_GET[self::$campaign_term]))
            $campaign[self::TERM] = $_GET[self::$campaign_term];
        if (isset($_GET[self::$campaign_content]))
            $campaign[self::CONTENT] = $_GET[self::$campaign_content];
        if (isset($_GET[self::$campaign_campaign]))
            $campaign[self::CAMPAIGN] = $_GET[self::$campaign_campaign];
        
        return new Referer($referer, $campaign);
        
    }
    
}