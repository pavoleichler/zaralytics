<?php

namespace Zaralytics\Transporters;

/**
 * @author Pavol Eichler
 */
class SocketTransporter implements ITransporter {
   
    /**
     * Socket connection protocol. Use ssl for a secured connection or leave null for a standard HTTP connection.
     * @var string
     */
    protected $protocol = null;
    
    /**
     * Host name.
     * @var string
     */
    protected $host = 'www.analyzuje.me';
    
    /**
     * Port number.
     * @var int
     */
    protected $port = 80;
    
    /**
     * Requested page path.
     * @var string
     */
    protected $path = '/track';
    
    /**
     * cURL timeout.
     * @var int 
     */
    protected $timeout = 3;

    /**
     * Sends the data to the remote service.
     * 
     * @param \Zaralytics\Project $project Project data.
     * @param \Zaralytics\Event $event Event data.
     * @param \Zaralytics\Referer $referer Referer data.
     * @param \Zaralytics\Request $request Request identifier.
     * @return bool
     */
    public function send(\Zaralytics\Project $project, \Zaralytics\Event $event, \Zaralytics\Referer $referer , \Zaralytics\Request $request = null) {
        
        $result = $this->post(array(
            'project' => $project->toJson(),
            'request' => ($request !== null) ? $request->getId() : null,
            'referer' => ($referer !== null) ? $referer->toJson() : null,
            'user' => $event->getUser()->toJson(),
            'category' => $event->getCategory(\Zaralytics\Event::FORMAT_JSON),
            'event' => $event->getName(),
            'values' => json_encode($event->getValues())
        ));
        
        return $result;
        
    }
    
    /**
     * Creates the HTTP request string based on the provided data.
     * 
     * @param array $data
     * @return string
     */
    protected function createHttpRequest($data) {
        
        // create the x-www-form-urlencoded string
        $body = "";
        foreach($data as $name => $value){
            $body .= urlencode($name) . "=" . urlencode($value) . "&";
        }
        $body = substr($body, 0, -1);
        
        // HTTP headers
        $request = "POST {$this->path} HTTP/1.1\r\n";
        $request .= "Host: {$this->host}\r\n";
        $request .= "Content-Type: application/x-www-form-urlencoded; charset=utf-8\r\n";
        $request .= "Content-Length: " . strlen($body) . "\r\n";
        $request .= "Connection: close\r\n";
        $request .= "\r\n";
        
        // HTTP body
        $request .= $body;
        
        return $request;
        
    }
    
    /**
     * Posts the provided data to the remote service URL.
     * 
     * @param array $data An associative array of data to post.
     * @return Remote service response
     * @throws \Exception If the cURL request fails.
     */
    protected function post($data) {
        
        try {

            // open a socket to the service server
            $socket = @fsockopen(($this->protocol ? $this->protocol . '://' : '') . $this->host, $this->port,
                                $errno, $errstr, $this->timeout);

            // check the provided socket
            if ($socket === false){
                throw new \Exception('Cannot open a socket connection');
            }
            
            // send the HTTP request
            fwrite($socket, $this->createHttpRequest($data));

            // close the connection
            fclose($socket);
            
        } catch (\Exception $exc) {
            
            // an error has occured
            return false;
            
        }

        // the request was sent fine
        return true;
        
    }
    
}