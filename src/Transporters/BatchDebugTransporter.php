<?php

namespace Zaralytics\Transporters;

/**
 * @author Pavol Eichler
 */
class BatchDebugTransporter extends BatchTransporter implements ITransporter {
   
    /**
     * Remote service URL.
     * @var string
     */
    protected $serviceUrl = 'http://www.analyzuje.me/track-batch';
    
    /**
     * cURL timeout.
     * @var int 
     */
    protected $timeout = 10;
    
    /**
     * Get the service URL.
     * 
     * @return string
     */
    public function getServiceUrl() {
        
        return $this->serviceUrl;
        
    }

    /**
     * Set the service URL.
     * 
     * @param string $serviceUrl
     */
    public function setServiceUrl($serviceUrl) {
        
        $this->serviceUrl = $serviceUrl;
        
    }
    
    /**
     * Posts all data from the temporary file to the remote service.
     * 
     * @param string $file A file name.
     * @param resource $stream A file resource.
     * @return Remote service response
     * @throws \Exception If the cURL request fails.
     */
    protected function post($file, $stream) {
        
        // open a connection
        $ch = curl_init();

        // read the whole file line by line (event by event)
        $post = '';
        while (($line = fgets($stream)) !== false) {
            $post .= $line;
        }
        
        // configure the request
        curl_setopt($ch, CURLOPT_URL, $this->serviceUrl);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
        curl_setopt($ch, CURLOPT_TIMEOUT, $this->timeout);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        // set the correct content type and make sure the current server time is send with the reqeust
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            "Content-Type: text/plain",
            "Date: " . gmdate("D, d M Y H:i:s", time()) . " GMT"
        ));
        
        // turn verbose output on
        curl_setopt($ch, CURLOPT_VERBOSE, true);
        // capture output
        $log = fopen('php://temp', 'rw+');
        curl_setopt($ch, CURLOPT_STDERR, $log);
        
        // execute the request
        $result = curl_exec($ch);

        // get the response code
        $code = curl_getinfo($ch , CURLINFO_HTTP_CODE);

        // close the connection
        curl_close($ch);

        if ($code !== 200){
            
            // dump the connection log
            //rewind($log); echo stream_get_contents($log);
            
            // forward the request response body
            echo $result; exit;
            
            throw new \Exception("cURL request failed.");
        }
        
        return $result;
        
    }
    
}