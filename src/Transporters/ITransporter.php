<?php

namespace Zaralytics\Transporters;

/**
 * @author Pavol Eichler
 */
interface ITransporter {

    /**
     * Notifies the transporter about data to be sent to the remote service.
     */
    public function send(\Zaralytics\Project $project, \Zaralytics\Event $event, \Zaralytics\Referer $referer, \Zaralytics\Request $requestId = null);
    
}