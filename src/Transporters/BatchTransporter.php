<?php

namespace Zaralytics\Transporters;

/**
 * @author Pavol Eichler
 */
abstract class BatchTransporter implements ITransporter {
   
    /**
     * Temporary directory path.
     * @var string
     */
    protected $tempDir;
   
    /**
     * Batch file name.
     * @var string
     */
    protected $bufferFile = 'buffer';
   
    /**
     * Status file name.
     * @var string
     */
    protected $statusFile = 'status';

    /**
     * Maximum buffering time.
     * @var int
     */
    protected $maxBufferTime = '6 hours';
    /**
     * Maximum buffered events.
     * @var int
     */
    protected $maxBufferEvents = 200;

    /**
     * 
     * @param string $tempDir Temporary directory path.
     */
    public function __construct($tempDir) {
        
        $this->tempDir = $tempDir;
        
    }

    /**
     * Sends the data to the remote service.
     * 
     * @param \Zaralytics\Project $project Project data.
     * @param \Zaralytics\Event $event Event data.
     * @param \Zaralytics\Referer $referer Referer data.
     * @param \Zaralytics\Request $request Request identifier.
     * @return bool
     */
    public function send(\Zaralytics\Project $project, \Zaralytics\Event $event, \Zaralytics\Referer $referer , \Zaralytics\Request $request = null) {
        
        $result = $this->save(array(
            'project' => $project->toJson(),
            'request' => ($request !== null) ? $request->getId() : null,
            'referer' => ($referer !== null) ? $referer->toJson() : null,
            'user' => $event->getUser()->toJson(),
            'category' => $event->getCategory(\Zaralytics\Event::FORMAT_JSON),
            'event' => $event->getName(),
            'values' => json_encode($event->getValues()),
            'created' => time()
        ));
        
        try {
            
            // get state information
            $events = $this->getBufferedEvents();
            $lastFlush = $this->getLastSuccesfulFlush();
            
            // if the max buffer events count or the max buffer time have been reached, flush
            if ($events >= $this->maxBufferEvents OR
                $lastFlush === null OR
                $lastFlush->add(\DateInterval::createFromDateString($this->maxBufferTime)) <= new \DateTime){
                
                $this->flush();
                
            }
            
        } catch (\Exception $exc) {
            // ignore flush errors
        }
        
        return $result;
        
    }
    
    /**
     * Saves the event data to a temporary file
     * 
     * @param type $data
     */
    protected function save($data) {
        
        // save the data
        $result = file_put_contents($this->getTempDir() . '/' . $this->bufferFile, json_encode($data) . "\r\n", FILE_APPEND|LOCK_EX);
        
        // track the number of buffered events
        $this->incrementBufferedEvents();
        
        return $result;
        
    }
    
    /**
     * Send all data from the temporary file to the tracking server and reset the temporary file.
     */
    public function flush() {
        
        // track the last flush attempt date
        $this->setLastFlushAttempt(new \DateTime);
        
        // open the temporary file
        $data = @fopen($this->getTempDir() . '/' . $this->bufferFile, 'r+');

        // check if the file could be opened
        if ($data === false AND file_exists($this->getTempDir() . '/' . $this->bufferFile)){
            throw new \Exception('Could not open the file.');
        }
        
        // get an exclusive lock
        flock($data, LOCK_EX);
        
        // send the data
        $sent = $this->post($this->getTempDir() . '/' . $this->bufferFile, $data);
        
        // check if the data were sent
        if (!$sent){
            // release the lock and close the file
            fclose($data);
            throw new \Exception('Could not send data.');
        }
        
        // truncate the file
        $result = ftruncate($data, 0);
        
        // reset the buffered events count
        $this->setBufferedEvents(0);
        
        // release the lock and close the file
        fclose($data);
        
        // track the last succesful flush date
        $this->setLastSuccesfulFlush(new \DateTime);
        
    }
    
    abstract protected function post($name, $stream);

    /**
     * Get the temp directory path and make sure it exists.
     * 
     * @return string
     */
    protected function getTempDir() {
        
        if (!file_exists($this->tempDir)) {
            mkdir($this->tempDir, 0777, true);
        }
        
        return $this->tempDir;
        
    }
    
    /**
     * Set the datetime of the last flush attempt.
     * 
     * @param \DateTime $datetime
     * @return \DateTime
     */
    protected function setLastFlushAttempt(\DateTime $datetime) {
        
        $status = $this->getStatus();
        
        $status->lastFlushAttempt = $datetime->format('U');
        
        $this->setStatus($status);
        
        return $status->lastFlushAttempt;
        
    }
    
    /**
     * Get the datetime of the last flush attempt.
     * 
     * @return \DateTime
     */
    protected function getLastFlushAttempt() {
        
        $status = $this->getStatus();
        
        return isset($status->lastFlushAttempt) ? new \DateTime('@' . $status->lastFlushAttempt) : null;
        
    }
    
    /**
     * Set the datetime of the last succesful flush.
     * 
     * @param \DateTime $datetime
     * @return \DateTime
     */
    protected function setLastSuccesfulFlush(\DateTime $datetime) {
        
        $status = $this->getStatus();
        
        $status->lastSuccesfulFlush = $datetime->format('U');
        
        $this->setStatus($status);
        
        return $status->lastSuccesfulFlush;
        
    }
    
    /**
     * Get the datetime of the last succesful flush.
     * 
     * @return \DateTime
     */
    protected function getLastSuccesfulFlush() {
        
        $status = $this->getStatus();
        
        return isset($status->lastSuccesfulFlush) ? new \DateTime('@' . $status->lastSuccesfulFlush) : null;
        
    }
    
    /**
     * Set the current number of events in the buffer.
     * 
     * @param int $count
     * @return int
     */
    protected function setBufferedEvents($count) {
        
        $status = $this->getStatus();
        
        $status->bufferedEvents = $count;
        
        $this->setStatus($status);
        
        return $status->bufferedEvents;
        
    }
    
    /**
     * Get the current number of events in the buffer.
     * 
     * @return int
     */
    protected function getBufferedEvents() {
        
        $status = $this->getStatus();
        
        return isset($status->bufferedEvents) ? $status->bufferedEvents : null;
        
    }
    
    /**
     * Increment the current number of events in the buffer.
     * 
     * @return bool|int
     */
    protected function incrementBufferedEvents() {
        
        // open the status file
        $sf = @fopen($this->getTempDir() . '/' . $this->statusFile, 'a+');
        
        // check if the file could be opened
        if ($sf === false){
            return false;
        }
        
        // get an exclusive lock
        flock($sf, LOCK_EX);
        
        // read and parse the contents
        $size = filesize($this->getTempDir() . '/' . $this->statusFile);
        if ($size){
            $data = fread($sf, $size);
            $status = json_decode($data);
        }
        if (!isset($status) OR !is_object($status)){
            $status = (object) array();
        }
        
        // incerement the buffered events count
        $status->bufferedEvents = isset($status->bufferedEvents) ? $status->bufferedEvents + 1 : 1;
        
        // rewrite the file
        $truncate = ftruncate($sf, 0);
        if (!$truncate){
            fclose($sf);
            return false;
        }
        
        $fwrite = fwrite($sf, json_encode($status));
        if (!$fwrite){
            fclose($sf);
            return false;
        }
        
        // close and realease
        fclose($sf);
        
        return $status->bufferedEvents;
        
    }
    
    /**
     * Get the status object.
     * 
     * @return StdClass|null
     */
    protected function getStatus() {
        
        $data = @file_get_contents($this->getTempDir() . '/' . $this->statusFile);
        
        if ($data === false){
            return null;
        }
        
        $status = json_decode($data);
        
        return is_object($status) ? $status : (object) array();
        
    }
    
    /**
     * Set tje current status.
     * 
     * @param array|StdClass $status
     * @return bool|int
     */
    protected function setStatus($status) {
        
        $data = json_encode($status);
        
        return @file_put_contents($this->getTempDir() . '/' . $this->statusFile, $data, LOCK_EX);
        
    }
    
}