<?php

namespace Zaralytics\Transporters;

/**
 * @author Pavol Eichler
 */
class BatchSocketTransporter extends BatchTransporter implements ITransporter {
    
    /**
     * Socket connection protocol. Use ssl for a secured connection or leave null for a standard HTTP connection.
     * @var string
     */
    protected $protocol = null;
    
    /**
     * Host name.
     * @var string
     */
    protected $host = 'www.analyzuje.me';
    
    /**
     * Port number.
     * @var int
     */
    protected $port = 80;
    
    /**
     * Requested page path.
     * @var string
     */
    protected $path = '/track-batch';
    
    /**
     * cURL timeout.
     * @var int 
     */
    protected $timeout = 10;
    
    protected function createHttpRequestHeader($file) {
        
        // HTTP headers
        $request = "POST {$this->path} HTTP/1.1\r\n";
        $request .= "Host: {$this->host}\r\n";
        $request .= "Date: " . gmdate("D, d M Y H:i:s", time()) . " GMT\r\n";
        $request .= "Content-Type: text/plain; charset=utf-8\r\n";
        $request .= "Content-Length: " . filesize($file) . "\r\n";
        $request .= "Connection: close\r\n";
        $request .= "\r\n";
        
        return $request;
        
    }
    
    /**
     * Posts all data from the temporary file to the remote service.
     * 
     * @param string $file A file name.
     * @param resource $stream A file resource.
     * @return Remote service response
     * @throws \Exception If the cURL request fails.
     */
    protected function post($file, $stream) {
        
        try {

            // open a socket to the service server
            $socket = @fsockopen(($this->protocol ? $this->protocol . '://' : '') . $this->host, $this->port,
                                $errno, $errstr, $this->timeout);

            // check the provided socket
            if ($socket === false){
                throw new \Exception('Cannot open a socket connection');
            }
            
            // send the HTTP request header
            fwrite($socket, $this->createHttpRequestHeader($file));
            
            // read and send the whole file line by line (event by event), preventing large memory requirements
            while (($event = fgets($stream)) !== false) {
                fwrite($socket, $event);
            }
                   
            // close the stream
            fclose($socket);
            
        } catch (\Exception $exc) {
            
            // an error has occured
            return false;
            
        }
        
        // the request was sent fine
        return true;
        
    }
    
}