<?php

namespace Zaralytics\Transporters;

/**
 * @author Pavol Eichler
 */
class DebugTransporter implements ITransporter {
   
    /**
     * Remote service URL.
     * @var string
     */
    protected $serviceUrl = 'http://www.analyzuje.me/track';
    
    /**
     * cURL timeout.
     * @var int 
     */
    protected $timeout = 10;
    
    /**
     * Sends the data to the remote service.
     * 
     * @param \Zaralytics\Project $project Project data.
     * @param \Zaralytics\Event $event Event data.
     * @param \Zaralytics\Referer $referer Referer data.
     * @param \Zaralytics\Request $request Request identifier.
     * @return bool
     */
    public function send(\Zaralytics\Project $project, \Zaralytics\Event $event, \Zaralytics\Referer $referer , \Zaralytics\Request $request = null) {
        
        $result = $this->post(array(
            'project' => $project->toJson(),
            'request' => ($request !== null) ? $request->getId() : null,
            'referer' => ($referer !== null) ? $referer->toJson() : null,
            'user' => $event->getUser()->toJson(),
            'category' => $event->getCategory(\Zaralytics\Event::FORMAT_JSON),
            'event' => $event->getName(),
            'values' => json_encode($event->getValues())
        ));
        
        return ($result !== false) ? true : false;
        
    }
 
    /**
     * Get the service URL.
     * 
     * @return string
     */
    public function getServiceUrl() {
        
        return $this->serviceUrl;
        
    }

    /**
     * Set the service URL.
     * 
     * @param string $serviceUrl
     */
    public function setServiceUrl($serviceUrl) {
        
        $this->serviceUrl = $serviceUrl;
        
    }
    
    /**
     * Posts the provided data to the remote service URL.
     * 
     * @param array $data An associative array of data to post.
     * @return Remote service response
     * @throws \Exception If the cURL request fails.
     */
    protected function post($data) {
        
        // open a connection
        $ch = curl_init();

        // configure the request
        curl_setopt($ch, CURLOPT_URL, $this->serviceUrl);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_TIMEOUT, $this->timeout);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        // turn verbose output on
        curl_setopt($ch, CURLOPT_VERBOSE, true);
        // capture output
        $log = fopen('php://temp', 'rw+');
        curl_setopt($ch, CURLOPT_STDERR, $log);
        
        // execute the request
        $result = curl_exec($ch);

        // get the response code
        $code = curl_getinfo($ch , CURLINFO_HTTP_CODE);

        // close the connection
        curl_close($ch);
        
        if ($code !== 200){
            
            // dump the connection log
            //rewind($log); echo stream_get_contents($log);
            
            // forward the request response body
            echo $result; exit;
            
            throw new \Exception("cURL request failed.");
        }
        
        return $result;
        
    }
    
}