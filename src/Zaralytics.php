<?php

namespace Zaralytics;

/**
 * @author Pavol Eichler
 */
class Zaralytics {
    
    /**
     * @var Request
     */
    protected $request;
    /**
     * Current project.
     * @var Project
     */
    protected $project;
    /**
     * Current referer.
     * @var Referer
     */
    protected $referer;
    /**
     * Current user.
     * @var User
     */
    protected $user;
    
    /**
     * Data transporter.
     * @var Transporters\ITransporter
     */
    protected $transporter;

    /**
     * @param string $projectId Project ID.
     */
    public function __construct($projectId) {
        
        // assign project
        $this->project = new Project($projectId);
        
        // generate a unique request to identify all events of this object instance
        $this->request = new Request();
        
        // get referer data
        $this->referer = Referer::identify();
        
        // identify the current user
        $this->user = User::Identify();
        
    }
    
    /**
     * Track an event.
     * 
     * @param string|array $category Event category as a string ('login/facebook') or an array (array('login', 'facebook')).
     * @param string $event Event name.
     * @param array $values An associative array of event related data. String or nummeric data values are allowed only.
     * @return bool
     */
    public function track($category, $event, $values = array()) {
        
        $event = new Event($this->user, $category, $event, $values);
        
        return $this->getTransporter()->send($this->project, $event, $this->referer, $this->request);
        
    }
    
    /**
     * Get the current user.
     * 
     * @return User
     */
    public function getUser() {
        
        return $this->user;
        
    }

    /**
     * Get the current transporter.
     * 
     * @return Transporters\ITransporter
     */
    public function getTransporter() {
        
        // use a default transporter, if not set
        if ($this->transporter === null){
            $this->transporter = new Transporters\SocketTransporter;
        }
        
        return $this->transporter;
        
    }

    /**
     * Set the current uset.
     * 
     * @param User $user
     */
    public function setUser($user) {
        
        $this->user = $user;
        
    }

    /**
     * Set a transporter to use.
     * 
     * @param Transporters\ITransporter $transporter
     */
    public function setTransporter($transporter) {
        
        $this->transporter = $transporter;
        
    }
    
}