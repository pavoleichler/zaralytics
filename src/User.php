<?php

namespace Zaralytics;

/**
 * @author Pavol Eichler
 */
class User {
   
    /**
     * Cookie name.
     * 
     * @var string
     */
    protected static $cookie = '_zau';
    /**
     * Cookie expiration time.
     * @var int
     */
    protected static $ttl = 157680000; // 5 years

    /**
     * Current session ID.
     * 
     * @var string
     */
    protected $sessionId;
    /**
     * Current user agent.
     * 
     * @var string
     */
    protected $userAgent;

    /**
     * @param string $sessionId
     * @param string $userAgent
     */
    public function __construct($sessionId, $userAgent = null) {
        
        $this->sessionId = $sessionId;
        $this->userAgent = $userAgent;
        
    }
    
    /**
     * Get the current session ID.
     * 
     * @return string
     */
    public function getSessionId() {
        
        return $this->sessionId;
        
    }
    
    /**
     * Get the current user agent.
     * 
     * @return string
     */
    public function getUserAgent() {
        
        return $this->sessionId;
        
    }
    
    /**
     * Returns all object data as a JSON string.
     * 
     * @return string
     */
    public function toJson() {
        
        return json_encode(array(
            'sessionId' => $this->sessionId,
            'userAgent' => $this->userAgent
        ));
        
    }
    
    /**
     * Identifies the current user based on HTTP request data.
     * 
     * @return User
     */
    public static function identify(){
        
        if (isset($_COOKIE[self::$cookie]) AND
            self::validateSessionId($_COOKIE[self::$cookie])){
            // we have a valid user identified by a cookie
            $sessionId = $_COOKIE[self::$cookie];
        }else{
            // we do not have an identified user yet
            $sessionId = self::generateSessionId();
        }
        
        // renew the cookie
        // get the domain name
        $host = isset($_SERVER['HTTP_HOST']) ? $_SERVER['HTTP_HOST'] : $_SERVER['SERVER_NAME'];
        $valid = preg_match('/[^.]+\.[^.]+$/', $host, $matches);
        $domain = $valid ? $matches[0] : null;
        // set the cookie
        setcookie(self::$cookie, $sessionId, time() + self::$ttl, '/', '.' . $domain);
        
        // identify the user agent
        $userAgent = isset($_SERVER['HTTP_USER_AGENT']) ? $_SERVER['HTTP_USER_AGENT'] : null;
        
        return new User($sessionId, $userAgent);
        
    }
    
    /**
     * Validates the session ID format.
     * 
     * @param string $sessionId
     * @return bool
     */
    protected static function validateSessionId($sessionId) {
        
       return (bool) preg_match('/[\d]+\.[\da-f]{13,}\.[\da-f]{8,}/', $sessionId);
        
    }
    
    /**
     * Generates a new pseudo-unique session ID.
     * 
     * @return string
     */
    protected static function generateSessionId() {
        
        return uniqid(mt_rand() . '.', true);
        
    }
    
}